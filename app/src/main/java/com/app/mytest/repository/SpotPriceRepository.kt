package com.app.mytest.repository

import com.app.mytest.models.signup.SpotPriceResponse
import com.app.mytest.service.ApiService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class SpotPriceRepository {

    fun getSpotPrice(): Observable<Response<SpotPriceResponse>> {
        return ApiService.getSpotPrice().spotPrice()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
    }
}