package com.app.mytest.repository

import com.app.mytest.models.signup.SignUpRequest
import com.app.mytest.models.signup.SignUpResponse
import com.app.mytest.service.ApiService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class SignUpRepository {

    fun signUp(signUpRequest: SignUpRequest): Observable<Response<SignUpResponse>> {
        return ApiService.signUpService().signup(signUpRequest)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
    }
}