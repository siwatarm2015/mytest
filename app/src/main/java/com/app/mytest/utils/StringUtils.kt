package com.app.mytest.utils

import java.util.*


fun randomString(len: Int): String {

    val random = Random()
    val data = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    val sb = StringBuilder(len)

    for (i in 0 until len) {
        sb.append(data[random.nextInt(data.length)])
    }
    return sb.toString()
}