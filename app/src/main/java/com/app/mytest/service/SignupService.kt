package com.app.mytest.service

import com.app.mytest.models.signup.SignUpRequest
import com.app.mytest.models.signup.SignUpResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface SignupService {

    @POST("/api/v3/users/register.json")
    fun signup(
        @Body signUpRequest: SignUpRequest
    ): Observable<Response<SignUpResponse>>
}