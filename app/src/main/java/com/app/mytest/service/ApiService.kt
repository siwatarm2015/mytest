package com.app.mytest.service

class ApiService{
    companion object {
        fun signUpService(): SignupService {
            return RetrofitConnection.create().create(SignupService::class.java)
        }

        fun getSpotPrice(): SpotPriceService {
            return RetrofitConnection.create().create(SpotPriceService::class.java)
        }

    }


}