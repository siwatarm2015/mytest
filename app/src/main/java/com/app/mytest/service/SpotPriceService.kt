package com.app.mytest.service

import com.app.mytest.models.signup.SpotPriceResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface SpotPriceService {

    @GET("/api/v2/spot_price.json")
    fun spotPrice(): Observable<Response<SpotPriceResponse>>
}