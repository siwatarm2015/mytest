package com.app.mytest.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit

class RetrofitConnection(){

    companion object {

        fun create(): Retrofit {

            val builder = GsonBuilder()
                .setLenient()
                .create()


            val okHttpClient = OkHttpClient.Builder().apply {
                readTimeout(30, TimeUnit.SECONDS)
            }
            return Retrofit.Builder().apply {
                baseUrl("https://staging.hellogold.com/")
                addConverterFactory(GsonConverterFactory.create(builder))
                addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                client(okHttpClient.build())
            }.build()

        }

    }



}