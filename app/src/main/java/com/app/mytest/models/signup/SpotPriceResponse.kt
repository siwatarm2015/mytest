package com.app.mytest.models.signup

import com.google.gson.annotations.SerializedName

class SpotPriceResponse{

    @SerializedName("result")
    var result: String? = null
    @SerializedName("data")
    var data: SpotPrice? = null

    inner class SpotPrice{
        @SerializedName("buy")
        var buy: Double? = null
        @SerializedName("sell")
        var sell: Double? = null
        @SerializedName("spot_price")
        var spot_price: Double? = null
        @SerializedName("timestamp")
        var timestamp: String? = null
    }

}