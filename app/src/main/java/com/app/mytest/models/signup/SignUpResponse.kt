package com.app.mytest.models.signup

import com.google.gson.annotations.SerializedName

data class SignUpResponse (@SerializedName("result")
                           var result: String,
                           @SerializedName("data")
                           var data: UserData,
                           @SerializedName("code")
                           var code: String
)