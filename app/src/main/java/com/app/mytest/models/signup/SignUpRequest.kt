package com.app.mytest.models.signup

import com.google.gson.annotations.SerializedName

data class SignUpRequest(
    @SerializedName("email")
    var email: String,
    @SerializedName("uuid")
    var uuid: String,
    @SerializedName("data")
    var data: String,
    @SerializedName("tnc")
    var tnc: String
    )