package com.app.mytest.models.signup

import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("api_token")
    var api_token: String,
    @SerializedName("public_key")
    var public_key: String,
    @SerializedName("api_key")
    var api_key: String,
    @SerializedName("account_number")
    var account_number: String
)