package com.app.mytest.activity.main

interface MainContract {

    interface MainView {

        fun getUserEmail(): CharSequence

        fun showLoading()

        fun hideLoading()

        fun navigateToDashBoard()

        fun showToast()

        fun showToastEmail()

        fun setText()

        fun disableButton()

        fun enableButtun()

        fun getCheckbox() : Boolean
    }

    interface MainPresenter {

        fun signUp()

        fun onSignUpClicked()

        fun isValidEmail()
    }



}