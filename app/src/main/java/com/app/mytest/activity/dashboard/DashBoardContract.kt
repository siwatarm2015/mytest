package com.app.mytest.activity.dashboard

import com.app.mytest.models.signup.SpotPriceResponse
import java.util.ArrayList

interface DashBoardContract {

    interface DashBoardView {

        fun setUserEmail()

        fun setSwipeColor()

        fun showRefreshing()

        fun hideRefreshing()

        fun showToast()

        fun showSpotPrice(spotPriceList: ArrayList<SpotPriceResponse.SpotPrice>)

        fun refreshSpotPrice(itemPrice: ArrayList<SpotPriceResponse.SpotPrice>)

    }

    interface DashBoardPresenter {

        fun getSpotPrice()

        fun onRefreshSpotPrice()

    }



}