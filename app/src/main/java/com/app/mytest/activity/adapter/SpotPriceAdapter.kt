package com.app.mytest.activity.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.mytest.R
import com.app.mytest.models.signup.SpotPriceResponse
import kotlinx.android.synthetic.main.item_spot_price.view.*

class SpotPriceAdapter(private var context: Context?) :
    RecyclerView.Adapter<ViewHolder>() {

    private var spotPrice = ArrayList<SpotPriceResponse.SpotPrice>()

    fun setSpotPrice(spotPrice: ArrayList<SpotPriceResponse.SpotPrice>){
        this.spotPrice = spotPrice
        notifyDataSetChanged()
    }

    fun clear() {
        spotPrice.clear()
        notifyDataSetChanged()
    }

    fun addAll(list: ArrayList<SpotPriceResponse.SpotPrice>) {
        spotPrice.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = spotPrice.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_spot_price, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val buy = context?.getString(R.string.buy) + spotPrice[position].buy.toString()
        val sell = context?.getString(R.string.sell) + spotPrice[position].sell.toString()
        val price = context?.getString(R.string.spot_price) + spotPrice[position].spot_price.toString()
        val time = context?.getString(R.string.time) + spotPrice[position].timestamp.toString()

        holder.txtBuy.text = buy
        holder.txtSell.text = sell
        holder.txtPrice.text = price
        holder.txtTime.text = time
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val txtSell: TextView = view.txtSell
    val txtPrice: TextView = view.txtSpotPrice
    val txtBuy: TextView = view.txtBuy
    val txtTime: TextView = view.txtTime
}