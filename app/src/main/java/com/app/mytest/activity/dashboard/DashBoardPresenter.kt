package com.app.mytest.activity.dashboard

import androidx.lifecycle.MutableLiveData
import com.app.mytest.activity.adapter.SpotPriceAdapter
import com.app.mytest.models.signup.SpotPriceResponse
import com.app.mytest.repository.SpotPriceRepository
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.*
import java.util.concurrent.TimeUnit

class DashBoardPresenter(private val view: DashBoardContract.DashBoardView) :
    DashBoardContract.DashBoardPresenter {

    private var spotPriceResponse: MutableLiveData<SpotPriceResponse> = MutableLiveData()
    private val spotPriceRepository = SpotPriceRepository()
    private var compositeDisposable = CompositeDisposable()
    private lateinit var adapter: SpotPriceAdapter
    private var isRefresh = false

    init {
        view.setUserEmail()
        view.setSwipeColor()
    }

    override fun getSpotPrice() {

        view.showRefreshing()

        val spotPriceList = ArrayList<SpotPriceResponse.SpotPrice>()

        Observable.interval(3, TimeUnit.SECONDS).subscribe {
            spotPriceRepository.getSpotPrice().apply {
                subscribe { response ->
                    when (response.code()) {
                        200 -> {
                            spotPriceResponse.value = response.body()
                            spotPriceResponse.value?.data?.let {
                                if (isRefresh) {
                                    spotPriceList.clear()
                                }
                                spotPriceList.add(it)
                                setSpotPriceList(spotPriceList)
                            }
                        }
                        else -> {
                            isRefresh = false
                            view.hideRefreshing()
                            view.showToast()
                        }
                    }
                }.addTo(compositeDisposable)
            }
        }.addTo(compositeDisposable)
    }

    private fun setSpotPriceList(spotPriceList: ArrayList<SpotPriceResponse.SpotPrice>) {

        view.hideRefreshing()

        view.showSpotPrice(spotPriceList)

        when (isRefresh) {
            true -> {
                val itemPrice = ArrayList<SpotPriceResponse.SpotPrice>()
                itemPrice.addAll(spotPriceList)
                view.refreshSpotPrice(itemPrice)
                isRefresh = false
            }
        }

        when (spotPriceList.size) {
            10 -> compositeDisposable.clear()
        }

    }

    override fun onRefreshSpotPrice() {
        compositeDisposable.clear()
        isRefresh = true
        getSpotPrice()
    }
}