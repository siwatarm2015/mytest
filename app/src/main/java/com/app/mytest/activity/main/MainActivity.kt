package com.app.mytest.activity.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.mytest.R
import com.app.mytest.activity.dashboard.DashBoardActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), MainContract.MainView {

    private var compositeDisposable = CompositeDisposable()
    private lateinit var presenter: MainContract.MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)

    }

    fun signUpClick(view: View) {

        presenter.onSignUpClicked()
    }

    override fun getUserEmail(): CharSequence {
        return edtEmail.text
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun navigateToDashBoard() {
        Intent(this@MainActivity, DashBoardActivity::class.java).apply {
            startActivity(this)
        }
    }

    override fun setText() {
        edtEmail.setText("")
    }

    override fun showToast() {
        Toast.makeText(this, "Some Thing Wrong", Toast.LENGTH_SHORT).show()
    }

    override fun showToastEmail() {
        Toast.makeText(this,"Invalid Email", Toast.LENGTH_SHORT).show()

    }

    override fun getCheckbox(): Boolean {
       return chkTnc.isChecked
    }

    override fun disableButton() {
        btnSignup.isEnabled = false
    }

    override fun enableButtun() {
        btnSignup.isEnabled = true
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
