package com.app.mytest.activity.dashboard

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.mytest.R
import com.app.mytest.activity.adapter.SpotPriceAdapter
import com.app.mytest.models.signup.SpotPriceResponse
import com.orhanobut.hawk.Hawk
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_dash_board.*

class DashBoardActivity : AppCompatActivity(), DashBoardContract.DashBoardView {

    private var compositeDisposable = CompositeDisposable()
    private lateinit var presenter: DashBoardContract.DashBoardPresenter
    private var isStart = false
    private lateinit var adapter: SpotPriceAdapter


    override fun onResume() {
        super.onResume()
        when (isStart) {
            true -> {
                presenter.getSpotPrice()
                isStart = false
            }
            false -> {
                presenter.onRefreshSpotPrice()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)

        adapter = SpotPriceAdapter(this)
        rvItems.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvItems.adapter = adapter

        presenter = DashBoardPresenter(this)

        btnRefresh.setOnClickListener {
            presenter.onRefreshSpotPrice()
        }

        swipeContainer.setOnRefreshListener {
            presenter.onRefreshSpotPrice()
        }

    }

    override fun setSwipeColor() {
        swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
    }

    override fun showRefreshing(){
        swipeContainer.isRefreshing = true
    }

    override fun hideRefreshing(){
        swipeContainer.isRefreshing = false
    }

    override fun showSpotPrice(spotPriceList: ArrayList<SpotPriceResponse.SpotPrice>) {
        adapter.setSpotPrice(spotPriceList)
    }

    override fun refreshSpotPrice(itemPrice: ArrayList<SpotPriceResponse.SpotPrice>) {
        adapter.clear()
        adapter.addAll(itemPrice)
        swipeContainer.isRefreshing = false
    }

    override fun showToast() {
        Toast.makeText(this, "Some Thing Wrong", Toast.LENGTH_SHORT).show()
    }

    override fun setUserEmail() {
        val email = getString(R.string.email_title) + Hawk.get("Email", "-")
        txtEmail.text = email
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
