package com.app.mytest.activity.main

import android.text.TextUtils
import android.util.Patterns
import com.app.mytest.models.signup.SignUpRequest
import com.app.mytest.repository.SignUpRepository
import com.app.mytest.utils.randomString
import com.orhanobut.hawk.Hawk
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.*

class MainPresenter(private val view: MainContract.MainView) :
    MainContract.MainPresenter {

    private var signUpRepository = SignUpRepository()
    private var compositeDisposable = CompositeDisposable()

//    override fun isValidEmail(): Boolean {
//        val target = view.getUserEmail()
//        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
//    }

    override fun isValidEmail() {
        val target = view.getUserEmail()
        if (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()) {
            signUp()
        } else {
            view.setText()
            view.showToastEmail()
        }
    }

    override fun signUp() {

        val uuid = UUID.randomUUID().toString()
        val tnc = view.getCheckbox()
        val data = randomString(10)
        val email = view.getUserEmail().toString()

        view.disableButton()
        view.showLoading()
        signUpRepository.signUp(SignUpRequest(email, uuid, data, tnc.toString())).apply {
            subscribe { response ->
                when (response.code()) {
                    200 -> {
                        view.enableButtun()
                        view.hideLoading()
                        Hawk.put("Email", email)
                        view.navigateToDashBoard()
                    }
                    else -> {
                        view.enableButtun()
                        view.hideLoading()
                        view.showToast()
                    }
                }

            }.addTo(compositeDisposable)
        }

    }

    override fun onSignUpClicked() {
        isValidEmail()
    }
}