package com.app.mytest

import com.app.mytest.activity.main.MainContract
import com.app.mytest.activity.main.MainPresenter
import com.app.mytest.models.signup.SignUpRequest
import com.app.mytest.repository.SignUpRepository
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Response

class MainActivityTest {

    @Mock
    private lateinit var view: MainContract.MainView
    private lateinit var repository: SignUpRepository
    private lateinit var presenter: MainPresenter


    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        repository = SignUpRepository()
        presenter = MainPresenter(view)
    }

    @Test
    fun `Should show error when email is invalid` () {
        val s : CharSequence = "ssss"
        `when`(view.getUserEmail()).thenReturn(s)
        presenter.onSignUpClicked()
        verify(view).showToastEmail()
    }

    @Test
    fun `Should signUp Complete` () {

    }



}